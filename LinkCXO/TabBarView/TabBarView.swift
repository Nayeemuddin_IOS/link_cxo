//
//  TabBarView.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 16/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit


protocol TabbarViewDelegate {
    func didClickEventButton(eventButton : TabBarView )
}
class TabBarView: UIView {
    @IBOutlet var contentView: UIView!
    var delegate : TabbarViewDelegate?
    @IBOutlet weak var event_btn: MHPButton!
    @IBOutlet weak var network_btn: MHPButton!
    override init(frame: CGRect) {
          super.init(frame: frame)
          self.commonInit()
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          self.commonInit()
      }
    
    
    
   
    @IBAction func eventButtonClicked(_ sender: Any) {
       let viewController = Events_STORYBOARD.instantiateViewController(withIdentifier: "Event") as! EventViewController
             SwifterSwift.visibleViewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func inviteButtonClicked(_ sender: Any) {
        
        let viewController = INVITE_STORYBOARD.instantiateViewController(withIdentifier: "Invite") as! InviteViewController
        SwifterSwift.visibleViewController?.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func businessButtonClicked(_ sender: Any) {
        let viewController = BUSINESS_STORYBOARD.instantiateViewController(withIdentifier: "Business") as! BusinessViewController
                   SwifterSwift.visibleViewController?.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func clubButtonClicked(_ sender: Any) {
        let viewController = CLUB_STORYBOARD.instantiateViewController(withIdentifier: "Club") as! ClubViewController
                         SwifterSwift.visibleViewController?.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func networkButtonClicked(_ sender: Any) {
        
        let viewController = NETWORK_STORYBOARD.instantiateViewController(withIdentifier: "Network") as! NetworkViewController
            SwifterSwift.visibleViewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    
   private func commonInit(){
       Bundle.main.loadNibNamed("TabBarView", owner: self, options: nil)
       addSubview(contentView)
       contentView.frame = self.bounds
       contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
       
   }

}
