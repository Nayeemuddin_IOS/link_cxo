//
//  MHPColorUtils.swift
//  Malaicha
//
//  Created by vishnu khaitan on 07/09/18.
//  Copyright © 2018 Diasporatec LTD. All rights reserved.
//

import UIKit

class MHPColorUtils: NSObject {

}

struct MHPColors{
    struct Color{
        
        static let PRIMARY_BUTTON = UIColor(hexString: "FD9801")
        
        static let REMIT_PRIMARY_BUTTON = UIColor(hexString: "D92342")
        
        static let PRIMARY_BUTTON_PRESSED = UIColor(hexString: "F18200")
        
        static let PRIMARY_SUCCESS_BUTTON = UIColor(hexString: "01C804")
        
        static let PRIMARY_SUCCESS_BUTTON_PRESSED = UIColor(hexString: "0DA200")
        
        static let PRIMARY_BUTTON_DISABLED = UIColor(hexString: "C4C4C4")
        
        static let SECONDARY_BUTTON_DISABLED = UIColor(hexString: "C4C4C4", transparency: 0.2)
        
        static let PRIMARY_BLUE_100 = UIColor(hexString: "01417E")
        
        static let REMIT_PRIMARY_RED = UIColor(hexString: "C93847")
        
        static let MHPStrock_Color_Light = UIColor(hexString: "EBEBEB")
        
        static let MHPStrock_Color_Dark = UIColor(hexString: "C4C4C4")
        
        static let ErrorColor = UIColor(hexString: "FF0000")
        
        static let MHPTERTIARY_BROWN_100 = UIColor(hexString: "6C2611")
        
        static let HPTERTIARY_100 = UIColor(hexString: "001C38")
        
        static let HPTERTIARY_80 = UIColor(hexString: "001C38", transparency: 0.8)

        static let HPTERTIARY_60 = UIColor(hexString: "001C38", transparency: 0.6)
        static let HPTERTIARY_40 = UIColor(hexString: "001C38", transparency: 0.4)
        
        static let HPPRIMARY_40 = UIColor(hexString: "003F7F", transparency: 0.4)
        static let HPPRIMARY_60 = UIColor(hexString: "003F7F", transparency: 0.6)
        static let HPPRIMARY_80 = UIColor(hexString: "003F7F", transparency: 0.8)

        static let HPPRIMARY_100 = UIColor(hexString: "003F7F")

        static let HPTEXTINACTIVE = UIColor(hexString: "ABA9A7")
        
        static let HPTEXTACTIVE_BROWN = UIColor(hexString: "25201B")
        
        static let HPTEXTACTIVE_ORANGE = UIColor(hexString: "E47B01")
        
        static let HPLINKS_100 = UIColor(hexString: "0079FF")
        
        static let HPSecondaryDark_100 = UIColor(hexString: "B51E49")
        
        static let MHPGREEN = UIColor(hexString: "01C804")
        static let MHPPRIMARY_40 = UIColor(hexString: "EBEBEB", transparency: 1.0)
        
        static let BACKGROUD_COLOR = UIColor(hexString: "F9F9F9")
        
        static let CART_ITEM_WARNING_COLOR = UIColor(hexString: "FF0000", transparency: 0.04)
        
    }
    
    
}

