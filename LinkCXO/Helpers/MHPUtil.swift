//
//  Util.swift
//  Skeleton
//
//  Created by Systango on 01/06/17.
//  Copyright © 2017 Systango. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


class MHPUtil : NSObject {
    
    public static func postNotification(name: String, dict: Dictionary<String, Any>) {
        
        let notification: Notification = Notification.init(name: Notification.Name(rawValue: name), object: nil, userInfo: dict) as Notification
        NotificationCenter.default.post(notification)
    }
    
    // Is app using RTL language or not
    var isRTLEnabled : Bool {
        return false // TODO Vishnu
        //        get{
        //            #if DEBUG_DEV
        //            return false
        //            #else
        //            return true
        //            #endif
        //
        //        }
    }
    
    // Loading app Environment
    class func loadEnvironment() -> Environment
    {
        
        #if DEBUG_STAG
        return .STAGING
        #elseif DEBUG_PROD
        return .PROD
        #elseif DEBUG_DEV
        return .QA
        #elseif RELEASE_PROD
        return .PROD
        #else
        
        return .PROD
        #endif
        
    }
    
    // It generate shake animation to view
    class func shakeView(_ shakeView: UIView) {
        let shake = CABasicAnimation(keyPath: "position")
        let xDelta = CGFloat(5)
        shake.duration = 0.05
        shake.repeatCount = 5
        shake.autoreverses = true
        
        let from_point = CGPoint(x: shakeView.center.x - xDelta, y: shakeView.center.y)
        let from_value = NSValue(cgPoint: from_point)
        
        let to_point = CGPoint(x: shakeView.center.x + xDelta, y: shakeView.center.y)
        let to_value = NSValue(cgPoint: to_point)
        
        shake.fromValue = from_value
        shake.toValue = to_value
        shake.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        shakeView.layer.add(shake, forKey: "position")
        
    }
    
    // Setting Border Colors..
    class func setBorderCornerAndColor(_ view : UIView, cornerRadius : CGFloat = CORNER_RADIUS, borderColor : UIColor , borderWidth : CGFloat = 1, maskToBounds : Bool = false ) {
        
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.masksToBounds = maskToBounds
    }
    
    
public static func CollectionCityName(cityString :String) -> NSAttributedString {
        
 var attributedString = NSMutableAttributedString()
   attributedString = NSMutableAttributedString(string: "Not seeing your recipient? Your collection city is: ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        
        let cityString_attributted = NSAttributedString.init(string: cityString , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(cityString_attributted)
        
        
        let last_string = NSAttributedString.init(string: " Change your collection city to send to other recipients." , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(last_string)
        
        
        return attributedString
    }
    
    
    public static func MessageCityName() -> NSAttributedString {
           
    var attributedString = NSMutableAttributedString()
      attributedString = NSMutableAttributedString(string: "You have reached the maxium amount of recipient for your account", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
           
           let cityString_attributted = NSAttributedString.init(string: "You have" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
           attributedString.append(cityString_attributted)
        
        
        let onerecipient_attributted = NSAttributedString.init(string: "in your HP account and" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
                  attributedString.append(onerecipient_attributted)
           
           
          let rcipient_attributted = NSAttributedString.init(string: "& Recioinet" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
                attributedString.append(rcipient_attributted)
        
        
        let thir_attributted = NSAttributedString.init(string: "in Malacha account to delete a helolo pisa recipient care line:" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(thir_attributted)
        
        
        let oneattrbibyte = NSAttributedString.init(string: "0978787878" , attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue,kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.orange, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
       
              attributedString.append(oneattrbibyte)
        
        
      
       
           
           
           return attributedString
       }
    
    
    
    
    public static func collectionCityFooterMessage(cityString :String) -> NSAttributedString {
         
    var attributedString = NSMutableAttributedString() 
      attributedString = NSMutableAttributedString(string: "Don’t want to send to ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
           
           let cityString_attributted = NSAttributedString.init(string: cityString , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
           attributedString.append(cityString_attributted)
           
           
           let last_string = NSAttributedString.init(string: "? Change your collection city to send to other collection points." , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
           attributedString.append(last_string)
           
           
           return attributedString
       }
    

    
    
    public static func HistoryTypes(name: String, type : HistyoryType) -> NSAttributedString {
        
        switch type {
        case .Name:
            let attributedString = NSMutableAttributedString(string: "Recipient: ", attributes: [kCTFontAttributeName as NSAttributedString.Key : ralewaySemiBoldFontWithSize(18 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
            
             let secondAttributedString = NSMutableAttributedString(string: name, attributes: [kCTFontAttributeName as NSAttributedString.Key : ralewayExtraBoldFontWithSize(18 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
 
           
           attributedString.append(secondAttributedString)
             return attributedString
        
        case .Payment:
        let attributedString = NSMutableAttributedString(string: "Payment status: ", attributes: [kCTFontAttributeName as NSAttributedString.Key : regularFontWithSize(12 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
        
        
        let secondAttributedString = NSMutableAttributedString(string: name, attributes: [kCTFontAttributeName as NSAttributedString.Key : boldFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : MHPColors.Color.ErrorColor!])

              attributedString.append(secondAttributedString)
                return attributedString

        case .Address:
           let attributedString = NSMutableAttributedString(string: "Pick Up At: ", attributes: [kCTFontAttributeName as NSAttributedString.Key : regularFontWithSize(12 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
           let secondAttributedString = NSMutableAttributedString(string: name, attributes: [kCTFontAttributeName as NSAttributedString.Key : boldFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
                 
                 attributedString.append(secondAttributedString)
                   return attributedString
        
        case .OrderStatus:
        let attributedString = NSMutableAttributedString(string: "Order status: ", attributes: [kCTFontAttributeName as NSAttributedString.Key : regularFontWithSize(12 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.white])
                let secondAttributedString = NSMutableAttributedString(string: name, attributes: [kCTFontAttributeName as NSAttributedString.Key : boldFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.white])
              attributedString.append(secondAttributedString)
            return attributedString
       
        }

    }
    
    
    public static func attributedStringHistoryTotal(total : String? ,inExchange : String? ) -> NSAttributedString? {
        
        var attributedString = NSMutableAttributedString()
        attributedString = NSMutableAttributedString(string:total ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : MHPColors.Color.PRIMARY_BUTTON!, kCTFontAttributeName as NSAttributedString.Key : ralewayExtraBoldFontWithSize(18 * ASPECT_RATIO)!])
        
        
        let attributes : [ NSAttributedString.Key : Any ] = [.font : lightFontWithSize(18 * ASPECT_RATIO)!,
                                                             .foregroundColor : MHPColors.Color.HPTEXTINACTIVE!,
                                                             .strokeWidth : 0]
        let seperator = NSAttributedString(string: " | ", attributes: attributes)
        
        attributedString.append(seperator)
        
        
        let attributes_ex : [ NSAttributedString.Key : Any ] = [.font : lightFontWithSize(14 * ASPECT_RATIO)!,
                                                                .foregroundColor : MHPColors.Color.HPTEXTINACTIVE!,
                                                                .strokeWidth : 0]
        let exchange = NSAttributedString(string: inExchange ?? "", attributes: attributes_ex)
        
        attributedString.append(exchange)
        
        
        return attributedString
        
        
    }
    
    
    public static func attributedStringForPayfromBank(amount : String? ) -> NSAttributedString? {
        
        var attributedString = NSMutableAttributedString()
        attributedString = NSMutableAttributedString(string:"Pay ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        
        let payAttributedString = NSMutableAttributedString(string: amount ?? "", attributes: [kCTFontAttributeName as NSAttributedString.Key : boldFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
        attributedString.append(payAttributedString)
        
        
        let lastAttributedString = NSMutableAttributedString(string: " from your bank account now?", attributes: [kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
        
        attributedString.append(lastAttributedString)
        
        
        return attributedString
        
    }
    
    public static func attributedStringForCurrentRate(rate : String?) -> NSAttributedString? {
    var attributedString = NSMutableAttributedString()
    attributedString = NSMutableAttributedString(string: "Current rate: ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
    
        let rateZAR = NSAttributedString.init(string:rate ?? "" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
             attributedString.append(rateZAR)
        
          return attributedString
        
    }
    
    
    public static func attributedStringForBankingAppConfirmation() -> NSAttributedString{
           
           var attributedString = NSMutableAttributedString()
           attributedString = NSMutableAttributedString(string:"Use your ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
           
           let payAttributedString = NSMutableAttributedString(string: "cellphone number and Malaicha.com Pin", attributes: [kCTFontAttributeName as NSAttributedString.Key : boldFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
           attributedString.append(payAttributedString)
           
           
           let lastAttributedString = NSMutableAttributedString(string: " to login into the hellopaisa app.", attributes: [kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!,NSAttributedString.Key.foregroundColor : UIColor.black])
           
           attributedString.append(lastAttributedString)
           
           
           return attributedString
           
       }
    
    
    
    
    
 public static func attributedStringForwareHouse(WareHouse : String? ,selectedWareHouse : String? ) -> NSAttributedString? {
        var attributedString = NSMutableAttributedString()
        attributedString = NSMutableAttributedString(string: "You are changing your collection city from ", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        
        let WareHouseAttributedString = NSAttributedString.init(string: WareHouse ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(WareHouseAttributedString)
        
        let toString = NSAttributedString.init(string: " to " , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        
      attributedString.append(toString)
        
        let selectedWareHouse = NSAttributedString.init(string: selectedWareHouse ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(selectedWareHouse)
        
    let string = NSAttributedString.init(string: " Some of your goods are not available and will be" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(string)
    
    let deleted = NSAttributedString.init(string: " deleted" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(deleted)
    
    let last_string = NSAttributedString.init(string: "  if you continue.Are you sure you want to change your city?" , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(last_string)

         return attributedString
        
    }
    
    
public static func attributedStringForPayFromBankWithOutstandingAmount(deducted : String? ,outStandingamount : String? ) -> NSAttributedString? {
    
    
   var attributedString = NSMutableAttributedString()
  
    if(deducted != nil){
        
        attributedString = NSMutableAttributedString(string: deducted!, attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
        
        
        let deductedString = NSAttributedString.init(string: " will be deducted from your bank account when available.\n\n " , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        
        attributedString.append(deductedString)
    }
    

    let deposit_string = NSAttributedString.init(string: "Deposit outstanding amount of " , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
        attributedString.append(deposit_string)
    
    
    let outstanding_amountString = NSAttributedString.init(string: outStandingamount ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])

    
     attributedString.append(outstanding_amountString)
    
    
    let last_string = NSAttributedString.init(string: " to settle this order." , attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : UIColor.black, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
          attributedString.append(last_string)
    

    
    
    return attributedString
    
    
    
    }
    
    
    
    func attributedStringForPrimaryBlue(with inputString : String?) -> NSAttributedString? {
        //NSForegroundColorAttributeName : color1, NSFontAttributeName:font
        
        if let subtitle = inputString{
            // *** Create instance of `NSMutableParagraphStyle`
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 2 // Whate
            
            
            let subTitleArray = subtitle.components(separatedBy: ":")
            
            if subTitleArray.count > 1 && subTitleArray[1].count > 0 {
                
                let firstPart : String? = "\(String(describing: (subTitleArray[0]))): "
                let secondPart : String? = subTitleArray[1]
                
                var attributedString = NSMutableAttributedString()
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                
                if let _firstPart = firstPart {
                    
                    attributedString = NSMutableAttributedString(string: _firstPart, attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : MHPColors.Color.HPTERTIARY_80!, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14 * ASPECT_RATIO)!])
                    
                } else {
                    
                    attributedString = NSMutableAttributedString(string: secondPart ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : MHPColors.Color.HPTERTIARY_80!, kCTFontAttributeName as NSAttributedString.Key : lightFontWithSize(14  * ASPECT_RATIO)!])
                    
                    return attributedString
                }
                
                let secondAttributedString = NSAttributedString.init(string: secondPart ?? "", attributes: [kCTForegroundColorAttributeName as NSAttributedString.Key : MHPColors.Color.HPTERTIARY_80!, kCTFontAttributeName as NSAttributedString.Key : mediumFontWithSize(14 * ASPECT_RATIO)!])
                
                attributedString.append(secondAttributedString)
                
                return attributedString
            }
            else{
                var attributedString = NSMutableAttributedString(string: subtitle)
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                return attributedString
            }
        }
        
        return nil
    }
    
    
    class func gotoAppstoreLink() {
        
        guard let url = URL(string: Constants.CONST_ITUNES_LINK) else  {
          return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
      }
      
    
    
      
    
   
    
    @objc
    func logout() {
        
      
       
    }
    
    @objc
    func logoutUserForSelfKYC() {
        //Clearing chat session:
      
       
    }
    
    
   
    
    
//    var shouldUpdateRefrenceData: Bool{
//        let refreshIntervel = MHPFirebaseHandler.fetchIntegerValueForKey(key: "REFERENCE_DATA_SYNC_INTERVAl_HOURS")
//        if let lastRefreshTime = Prefs.getLastReferencDataUpdateTime(){
//            let dateAfterRefreshIntervel = lastRefreshTime.dateAfterHours(hours: refreshIntervel.intValue)
//            if(dateAfterRefreshIntervel < Date()){
//                return true
//            }
//            return false
//        }
//        else{
//            return true
//        }
//
//    }
    
    
    
//    class func getParsedUrl(url: String, _ completion : @escaping ((String)-> (Void))) {
//        
//        if(url.contains("page.link") == false){
//            completion(url)
//            return
//        }
//        
//        if let url = URL(string: url){
//            // First handling dynamic lick Parsing
//            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(url) { (dynamiclink, error) in
//                print(dynamiclink?.url)
//                let minAppVersion = Float(dynamiclink?.minimumAppVersion ?? "0.0") ?? 0.0
//                let currentAppVersion = Float(Prefs.getAppVersion()) ?? 0.0
//                if(minAppVersion < currentAppVersion){
//                    return
//                }
//                if let urlString : String = dynamiclink?.url?.absoluteString{
//                    completion(urlString)
//                    return
//                }
//            }
//            // If Dynamic Link Parsing Failed handling URL directly
//            if(handled == false){
//                let urlString = url.absoluteString
//                completion(urlString)
//                return
//            }
//        }
//        
//    }
//    
    class func checkCameraAccess()->Bool{
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        if(cameraAuthorizationStatus == .denied){
          return false
        }
        return true
    }
    
    
    
    
}


func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}



func regularFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RobotoRegular.rawValue, size: size)
    return font
}

func boldFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RobotoBold.rawValue, size: size)
    return font
}

func mediumFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RobotoMedium.rawValue , size: size)
    return font
}

func lightFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RobotoLight.rawValue, size: size)
    return font
}

func ralewayRegularFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RobotoRegular.rawValue, size: size)
    return font
}

func ralewaySemiBoldFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RalewaySemiBold.rawValue, size: size)
    return font
}

func ralewayBoldFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RalewayBold.rawValue, size: size)
    return font
}

func ralewayExtraBoldFontWithSize (_ size: CGFloat) -> UIFont? {
    
    
    
    let usedFont = UIFont(name: FontName.RalewayExtraBold.rawValue, size: size)
    
    let uppercaseAttribs = [
                   UIFontDescriptor.FeatureKey.featureIdentifier: kNumberCaseType,
                   UIFontDescriptor.FeatureKey.typeIdentifier: kUpperCaseNumbersSelector
               ]
    
    let fontAttribs = [
        UIFontDescriptor.AttributeName.name: usedFont!.fontName,
               UIFontDescriptor.AttributeName.featureSettings: [uppercaseAttribs]
               ] as [UIFontDescriptor.AttributeName : Any]

           let descriptor = UIFontDescriptor(fontAttributes: fontAttribs)
    
    
    
    let font = UIFont(descriptor: descriptor, size: usedFont!.pointSize)
    
    
    return font
}



func ralewayMediumFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RalewayMedium.rawValue , size: size)
    return font
}

func ralewayLightFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FontName.RalewayLight.rawValue, size: size)
    return font
}

// For Localizations..

let kNotificationLanguageChanged        = NSNotification.Name(rawValue:"kNotificationLanguageChanged")

var localisator:Localisator
{
    return(Localisator.sharedInstance())
}

func localizedStringFor(_ key:String) -> String
{
    return(localisator.localizedString(forKey: key))
}

func Localization(_ string:String) -> String{
    return Localisator.sharedInstance().localizedString(forKey: string)
}

func SetLanguage(_ language:String) -> Bool {
    return Localisator.sharedInstance().setLanguage(language)
}

   

