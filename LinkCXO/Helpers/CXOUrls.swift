//
//  CXOUrls.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 13/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit


struct CXOConfig{
    
    struct ServiceURL{
        
        
        struct AuthenticationService{
            
            static var CUSTOMER_LOGIN = CXOUrls.SARB_V4_BASE_URL() +  "login"
            
            static var AGENT_LOGIN = CXOUrls.SARB_V4_BASE_URL() +  "login"
            
            static var VERIFY_OTP = CXOUrls.SARB_V4_BASE_URL() +  "verifyotp"
            
            static var RESEND_OTP = CXOUrls.SARB_V4_BASE_URL() +  "resendotp"
            
            static var VERIFY_CUSTOMER = CXOUrls.SARB_V4_BASE_URL() +  "verifyhellopaisacustomeragentassist"
            
            static var VERIFY_AGENT_LOGIN_OTP = CXOUrls.SARB_V4_BASE_URL() +  "verifyhellopaisacustomeragentassistotp"
            
            static var ACCEPT_TNC = CXOUrls.SARB_V4_BASE_URL() +  "tnc/malaicha"
            
            static var RESET_PIN = CXOUrls.SARB_V4_BASE_URL() +  "resetpin"
            
        }
        
        
        struct ShopService{
            
            static var GET_HOME_SCREEN_PRODUCT = CXOUrls.MALAICHA_BASE_URL() +  "api/v2/store/shop/\(Constants.LEGACY_SHOP_ID)/category/\(Constants.CATEGORY_ID)/product/\(Constants.PRODUCT_ID)"
            
             static var GET_PRODUCTS_BY_CATEGORYID = CXOUrls.MALAICHA_BASE_URL() +  "api/v2/store/shop/\(Constants.LEGACY_SHOP_ID)/category/%@/products"
            
            static var GET_WEEKLY_SPECIALS_CATEGORIES = CXOUrls.MALAICHA_BASE_URL() +  "api/v2/store/shop/\(Constants.LEGACY_SHOP_ID)/categories/\(Constants.WEEKLY_SPECIALS_ID)"
            
            static var GET_WAREHOUSES = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/shops/\(Constants.LEGACY_SHOP_ID)/warehouses"
           
            static var CHANG_WAREHOUSE = CXOUrls.SARB_V4_BASE_URL() +  "hellocustomerwarehouse"
            
            static var GET_CATEGORIES = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/shops/\(Constants.LEGACY_SHOP_ID)/categories"
            
            static var GET_PRODUCT_BY_PRODUCTID = CXOUrls.MALAICHA_BASE_URL() +  "api/v2/store/shop/\(Constants.LEGACY_SHOP_ID)/category/%@/product/%@?warehouse_id=%@"
            
             static var GET_PRODUCT_BY_PRODUCT_IDS = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/products?ids=%@&warehouse_id=%@"
            
            static var GET_ORDER_HISTORY_PRODUCT =
            CXOUrls.MALAICHA_BASE_URL() + "api/v2/store/shop/\(Constants.LEGACY_SHOP_ID)/order_details/%@"
            
    
         
        
        }
        
        struct CustomerService{
             
            static var GET_CUSTOMER_INFO = CXOUrls.SARB_V4_BASE_URL() +  "getcustomerinfo"
        }
        
        struct CallMeService{
            
            static var GET_CALLME_INFO = CXOUrls.SARB_V4_BASE_URL() +  "callme"
        }
        
        struct CartService{
            
            static var CHECK_LIMIT = CXOUrls.SARB_V4_BASE_URL() +  "limitcheck"
            
            static var GET_OFFER = CXOUrls.SARB_V4_BASE_URL() +  "getoffer"
             
            static var GET_CART_INFO = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/carts?shop_id=\(Constants.LEGACY_SHOP_ID)"
            
            static var DELETE_CART_ITEM = CXOUrls.MALAICHA_BASE_URL() + "api/v3/store/carts/%@/deleteCartItems?cartItemIds=%@"
            
            static var ADD_CART_ITEM = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/carts/%@/items/%@"
            
            static var ADD_NEW_CART_ITEM = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/carts/%@/items"
            
            static var CHANGE_CART_WAREHOUSE = CXOUrls.MALAICHA_BASE_URL() +  "api/v3/store/carts/%@/warehouses/change"
            
            
        }
        
        
        
        struct SearchService {
            
           static var SEARCH_ITEM = CXOUrls.MALAICHA_BASE_URL() +  "/api/v2/store/search/%@?shop_id=\(Constants.LEGACY_SHOP_ID)"
            
        }
        
        struct RecipientService{
            
            static var GET_BANKING_PARTNERS = CXOUrls.SARB_V4_BASE_URL() +  "getbankingpartners"
            
            static var GET_BENEFICIARIES = CXOUrls.SARB_V4_BASE_URL() +  "getbeneficiaries"
            
            static var DELETE_BENEFICIARY = CXOUrls.SARB_V4_BASE_URL() +  "deletebeneficiary"
            
            static var ADD_BENEFICIARY = CXOUrls.SARB_V4_BASE_URL() +  "addbeneficiary"

        }
        
        struct TransactionService{
            
             static var GET_TRANSACTIONS = CXOUrls.SARB_V4_BASE_URL() +  "gettransactions"
            
            static var GET_TRANSACTION_BY_ID = CXOUrls.SARB_V4_BASE_URL() +  "gettransaction"
    
        }
        
        
        struct CheckoutService{
            
             static var GET_DELIVERY_METHODS = CXOUrls.MALAICHA_BASE_URL() + "api/v3/store/shops/4/" +  "delivery-methods"
            
             static var ADD_TRANSACTION =  CXOUrls.SARB_V4_BASE_URL() +  "addtransaction"
            
        }
        
        struct ShopCategory{
            
            static var GET_SHOP_CATEGORY_METHODS = CXOUrls.MALAICHA_BASE_URL() + "api/v2/home/section"
            
            
        }
        
        
        struct RefrenceDataService{
            
            
        }
        
        
    }
}

class CXOUrls: NSObject {
    
   
    // Malaicha Base URL with respect to App Environment
    class func MALAICHA_BASE_URL() -> String {
        var baseUrl = ""
        switch CXOUtil.loadEnvironment() {
        case .SANDBOX: //TODO //NOT AVAILABLE
            baseUrl = ""
        case .QA:
            baseUrl = "https://malaichaqa.nvizible.co.za/"
        case .STAGING:
            baseUrl = "https://qmartdev.sbox.datafree.co.za/malaicha/"
        case .PROD:
            baseUrl = "https://qmart.datafree.co.za/"
        }
        return baseUrl
       
    }
    
    class func MALAICHA_CALL_REQUEST() -> String {
        var request = ""
        switch CXOUtil.loadEnvironment() {
        case .SANDBOX: //TODO //NOT AVAILABLE
            request = ""
        case .QA:
            request = "77"
        case .STAGING:
            request = "200"
        case .PROD:
            request = "236"
        }
        return request
       
    }
    
    
    class func SARB_V4_BASE_URL() -> String {
        var baseUrl = ""
        switch CXOUtil.loadEnvironment() {
               case .SANDBOX: //TODO //NOT AVAILABLE
                   baseUrl = ""
               case .QA:
                   baseUrl = CONST_QA_CXO_SVR
               case .STAGING:
                   baseUrl = CONST_STAG_CXO_SVR
               case .PROD:
                   baseUrl = CONST_LIVE_CXO_SVR
               }
       
        return baseUrl + "/SARB/v4/"
    }
    
    class func SELF_KYC_BASE_URL() -> String {
           var baseUrl = ""
           switch CXOUtil.loadEnvironment() {
                  case .SANDBOX: //TODO //NOT AVAILABLE
                      baseUrl = ""
                  case .QA:
                      baseUrl = ""
                  case .STAGING:
                      baseUrl = ""
                  case .PROD:
                      baseUrl = ""
                  }
          
           return baseUrl
       }
       
    
    
    
}
