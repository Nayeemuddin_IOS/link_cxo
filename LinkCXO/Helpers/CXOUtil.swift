//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 13/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


class CXOUtil : NSObject {
    
    public static func postNotification(name: String, dict: Dictionary<String, Any>) {
        
        let notification: Notification = Notification.init(name: Notification.Name(rawValue: name), object: nil, userInfo: dict) as Notification
        NotificationCenter.default.post(notification)
    }
    
    // Is app using RTL language or not
    var isRTLEnabled : Bool {
        return false 
    }
    
    // Loading app Environment
    class func loadEnvironment() -> Environment
    {
        
        #if DEBUG_STAG
        return .STAGING
        #elseif DEBUG_PROD
        return .PROD
        #elseif DEBUG_DEV
        return .QA
        #elseif RELEASE_PROD
        return .PROD
        #else
        
        return .PROD
        #endif
        
    }
    
    // It generate shake animation to view
    class func shakeView(_ shakeView: UIView) {
        let shake = CABasicAnimation(keyPath: "position")
        let xDelta = CGFloat(5)
        shake.duration = 0.05
        shake.repeatCount = 5
        shake.autoreverses = true
        
        let from_point = CGPoint(x: shakeView.center.x - xDelta, y: shakeView.center.y)
        let from_value = NSValue(cgPoint: from_point)
        
        let to_point = CGPoint(x: shakeView.center.x + xDelta, y: shakeView.center.y)
        let to_value = NSValue(cgPoint: to_point)
        
        shake.fromValue = from_value
        shake.toValue = to_value
        shake.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        shakeView.layer.add(shake, forKey: "position")
        
    }
    
    // Setting Border Colors..
    class func setBorderCornerAndColor(_ view : UIView, cornerRadius : CGFloat = CORNER_RADIUS, borderColor : UIColor , borderWidth : CGFloat = 1, maskToBounds : Bool = false ) {
        
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.masksToBounds = maskToBounds
    }
    
}

   

