//
//  Constants.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 13/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let ApplicationDelegate = UIApplication.shared.delegate as! AppDelegate

typealias CompletionHandler = (_ success: Bool, _ response: Any?) -> Void
typealias ProgressBlock = (_ progress : Progress) -> Void
typealias successBlock = (_ result:AnyObject) -> Void
typealias failureBlock = (_ result:Error) -> Void
typealias customfailureBlock = (_ result:AnyObject) -> Void
typealias failureBlockWithResponse = (_ result:Error, _ response : DataResponse<Any>? ) -> Void
typealias failureBlockWithDownloadResponse = (_ result:Error, _ response : DownloadResponse<Any>? ) -> Void

let DEVICE_BOUNDS = UIScreen.main.bounds
var ASPECT_RATIO = CGFloat(1.0)//DEVICE_BOUNDS.size.width/320

let WALKTHROUGH_STORYBOARD = UIStoryboard(name: "WalkThrough", bundle: nil)
let LOGIN_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let HOME_STORYBOARD = UIStoryboard(name: "Home", bundle: nil)
let Events_STORYBOARD = UIStoryboard(name: "Events", bundle: nil)
let BUSINESS_STORYBOARD = UIStoryboard(name: "Business", bundle: nil)
let CLUB_STORYBOARD = UIStoryboard(name: "Club", bundle: nil)
let NETWORK_STORYBOARD = UIStoryboard(name: "Network", bundle: nil)
let INVITE_STORYBOARD = UIStoryboard(name: "Invite", bundle: nil)



let CONST_QA_CXO_SVR   = ""

let CONST_STAG_CXO_SVR = ""

let CONST_LIVE_CXO_SVR = ""


//let CONST_DOCKER_LOCAL_INSTANCE = "http://127.0.0.1:8089"

//let CONST_ITUNES_LINK = "itms-apps://itunes.apple.com/us/app/apple-store/id1161658277?mt=8"

//let BASE_WIDTH    : CGFloat = 320.00

//let ASPECT_RATIO    : CGFloat = SwifterSwift.screenWidth/BASE_WIDTH

let CORNER_RADIUS : CGFloat = 6

let LOADING_RADIUS : CGFloat = 22

let LOADING_THICKNESS : CGFloat = 3

//let HPREWARDS_VERSION = "2"
//let PLATFORM_IOS = "iOS"


struct Constants {
    
    // MARK: General Constants
    
    static let TIME_CALCUATED_IN_SECONDS_A_DAY : UInt64 = 60*60*24
    
    static let LOGIN_SCREEN_TIMER_AFTER_WRONG_ATTEMPTS_IN_SECONDS : UInt64 = 2*60 // 2 Represents 2 mins here.
    
    static let DEFAULT_NO_OF_ATTEMPTS = 3
    
    static let DEFAULT_OPTED_IN_HPREWARDS = 100 // 100 is Reward Code which is 100 for coming soon screen.
    
    static let DEFAULT_MAX_FILE_SIZE_MB = 8
    
   

    static let CHANNEL = "linkCxo_ios_app"

    
    static let LEGACY_SHOP_ID = 4;
    static let CATEGORY_ID = 61;
    static let PRODUCT_ID = 3223;
    static let COUNTRY_ID = "2560";
    static let WEEKLY_SPECIALS_ID = 226;
    static let GROUP_ID = "3";
    static let HELLOPAISA_BANK_TYPE = "7";
    static let CURRENCY = "R";
    static let DOLLAR_SIGN = "$ ";
    static let BASE_FOREIGN_CURRENCY = "USD";
    static let COUNTRY_NAME = "Zimbabwe";
    static let LOCAL_CURRENCY = "ZAR";
    static let ONE_USD = "1 USD = ";
    static let TIMEOUT_MILLISESCONDS = 40000;
    static let SPACE = " ";
    static let APP_UPDATE_SKIP_TIME_INTERVEL = 6
    static let CONST_ITUNES_LINK = ""
    static let OFFER_EXPIRE_TIME = 300;
    
}


//typedef void (^completionBlock)(BOOL success, id response);

struct FONT_NAME {
    static let MHP_LIGHT_FONT = "GothamRounded-Light"
    static let MHP_BOOK_FONT = "GothamRounded-Book"
    static let MHP_MEDIUM_FONT = "GothamRounded-Medium"
    static let MHP_BOLD_FONT = "GothamRounded-Bold"
}

struct API_KEYS {
    
    static let GOOGLE_API_KEY: String = "YYYY"
    
    // Clever Tap keys..
    static let kCleverTapLIVEAccount: String = ""
    static let kCleverTapLIVEToken: String = ""
    
    static let kCleverTapDEVAccount: String = ""
    static let kCleverTapDEVToken: String = ""
    
    static let kCleverTapSANDBOXAccount: String = ""
    static let kCleverTapSANDBOXToken: String = ""
    
    static let kCleverTapSTAGINGAccount: String = ""
    static let kCleverTapSTAGINGToken: String = ""
    
    // UXCam keys
    static let UXCamQAToken: String = ""
    static let UXCamStagingToken: String = ""
    static let UXCamLiveToken: String = ""
}

// Audio Services Constants..

struct AudioServicesCodes  {
    static let KeyBoardClickSound : UInt32 = 1104
    static let Vibratation : UInt32 = 4095
}



