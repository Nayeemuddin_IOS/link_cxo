//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

enum MHPButtonType: Int {
    case PRIMARY_SUCCESS  = 0
    case PRIMARY_SUCCESS_PRESSED
    case PRIMARY_ENABLED
    case PRIMARY_PRESSED
    case PRIMARY_DISABLED
    case PRIMARY_ERROR
    case PRIMARY_ERROR_PRESSED
    case SECONDARY_ENABLED
    case SECONDARY_DISABLED
    case DEFAULT
}

@IBDesignable class MHPRoundButton: UIButton {
    
    override func awakeFromNib() {
        self.setFont()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.size.height / 2.0
        clipsToBounds = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override func prepareForInterfaceBuilder() {
        commonInit()
    }
    
    
    func commonInit(){
        self.refreshBackgroundColor(color: background!)
        self.refreshBorderWidth(width: borderWidthValue)
        self.refreshBorderColor(color: borderColor!)
        
        if (self.title(for: UIControl.State.normal) != nil){
            self.setLocalizedTitle(self.title(for: UIControl.State.normal)!, for: UIControl.State.normal)
        }
    }
    
    func refreshBackgroundColor(color : UIColor) {
        self.backgroundColor = color
    }
    
    func refreshBorderWidth(width : CGFloat) {
        self.layer.borderWidth = width
    }
    
    
    func refreshBorderColor(color : UIColor) {
         self.layer.borderColor = color.cgColor
    }
    
  
    func setLocalizedTitle(_ title: String, for state: UIControl.State) {
        
        self.setTitle(localizedStringFor(title), for: state)
        
    }

    
 
    
    @IBInspectable override var borderColor: UIColor? {
        didSet {
            self.refreshBorderColor(color: borderColor!)
        }
    }
    
    @IBInspectable var background: UIColor? = UIColor.white{
        didSet {
            self.refreshBackgroundColor(color: background!)
        }
    }
    
    @IBInspectable var borderWidthValue : CGFloat = 0 {
        didSet {
            refreshBorderWidth(width: borderWidthValue)
        }
    }
    
    func refreshCR(_value: CGFloat) {
        layer.cornerRadius = _value
    }

    public var ButtonStyle : MHPButtonType? {
        
        didSet {
            self.alpha = 1.0
            switch self.ButtonStyle! {
            case .PRIMARY_ENABLED:
                   self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON
                   self.titleLabel?.textColor = UIColor.white
            case .SECONDARY_ENABLED:
                self.backgroundColor = MHPColors.Color.REMIT_PRIMARY_RED
                self.titleLabel?.textColor = UIColor.white
                self.borderColor = MHPColors.Color.REMIT_PRIMARY_RED
            case .SECONDARY_DISABLED:
                 self.backgroundColor = MHPColors.Color.SECONDARY_BUTTON_DISABLED
                    self.borderColor = MHPColors.Color.SECONDARY_BUTTON_DISABLED
                    self.titleLabel?.textColor = UIColor.white
            case .PRIMARY_PRESSED:
                   self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON_PRESSED
            case .PRIMARY_SUCCESS:
                   self.backgroundColor = MHPColors.Color.PRIMARY_SUCCESS_BUTTON
            case .PRIMARY_SUCCESS_PRESSED:
                   self.backgroundColor = MHPColors.Color.PRIMARY_SUCCESS_BUTTON_PRESSED
            case .PRIMARY_DISABLED:
                self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON_DISABLED
                self.borderColor = MHPColors.Color.PRIMARY_BUTTON_DISABLED
                self.titleLabel?.textColor = UIColor.white
            case .PRIMARY_ERROR:
                self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON_DISABLED
            case .PRIMARY_ERROR_PRESSED:
                self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON_DISABLED
            case .DEFAULT:
                 self.backgroundColor = UIColor.white
            }
        }
    }

    
    var activityIndicator: UIActivityIndicatorView!
    var originalButtonText: String?


    func showLoading(title : String = "") {
        originalButtonText = self.titleLabel?.text
        if(title.length > 0){
            self.setTitle(title, for:.normal)
            if (activityIndicator == nil) {
                activityIndicator = createActivityIndicator()
            }
            self.showSpinning()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.showSpinning()
            })
        }
        else{
            if (activityIndicator == nil) {
                activityIndicator = createActivityIndicator()
            }
            showSpinning()
        }
    }

    func hideLoading() {
        self.setTitle(originalButtonText, for:.normal)
        activityIndicator.stopAnimating()
    }

    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.white
        return activityIndicator
    }

    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }

    private func centerActivityIndicatorInButton() {
        //print(self.titleLabel?.frame.minX)
       
        let leadingSpace = (self.titleLabel?.frame.minX)! - 30
        self.addConstraint(NSLayoutConstraint(item: self.activityIndicator, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant:leadingSpace))
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }

    func setFont() {
           
           
           if let usedFont:UIFont = self.titleLabel!.font{
                     //log.debug(DEVICE_BOUNDS.width)
                     let uppercaseAttribs = [
                         UIFontDescriptor.FeatureKey.featureIdentifier: kNumberCaseType,
                         UIFontDescriptor.FeatureKey.typeIdentifier: kUpperCaseNumbersSelector
                     ]

                     let fontAttribs = [
                         UIFontDescriptor.AttributeName.name: usedFont.fontName,
                         UIFontDescriptor.AttributeName.featureSettings: [uppercaseAttribs]
                         ] as [UIFontDescriptor.AttributeName : Any]

                     let descriptor = UIFontDescriptor(fontAttributes: fontAttribs)
                     self.titleLabel!.font = UIFont(descriptor: descriptor, size: usedFont.pointSize)
                 }
    }
  

}
