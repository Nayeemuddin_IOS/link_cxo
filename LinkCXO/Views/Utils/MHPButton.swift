//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

class MHPButton: UIButton {
    
    override func awakeFromNib() {
        self.setFont()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        
        if(MHPUtil().isRTLEnabled){
            if let image = self.image(for: UIControl.State.normal){
                self.setImage(image.imageFlippedForRightToLeftLayoutDirection(), for: UIControl.State.normal)
            }
            if(self.contentHorizontalAlignment == .left){
               self.contentHorizontalAlignment = .right
            }
        }
        
        if (self.title(for: UIControl.State.normal) != nil){
            self.setLocalizedTitle(self.title(for: UIControl.State.normal)!, for: UIControl.State.normal)
        }
        
    }
    
    func setLocalizedTitle(_ title: String, for state: UIControl.State = .normal) {
        let marginText = " \(localizedStringFor(title)) "
        self.setTitle(marginText, for: state)
    }

    
    func setFont() {
        
        
        if let usedFont:UIFont = self.titleLabel!.font{
                  //log.debug(DEVICE_BOUNDS.width)
                  let uppercaseAttribs = [
                      UIFontDescriptor.FeatureKey.featureIdentifier: kNumberCaseType,
                      UIFontDescriptor.FeatureKey.typeIdentifier: kUpperCaseNumbersSelector
                  ]

                  let fontAttribs = [
                      UIFontDescriptor.AttributeName.name: usedFont.fontName,
                      UIFontDescriptor.AttributeName.featureSettings: [uppercaseAttribs]
                      ] as [UIFontDescriptor.AttributeName : Any]

                  let descriptor = UIFontDescriptor(fontAttributes: fontAttribs)
                  self.titleLabel!.font = UIFont(descriptor: descriptor, size: usedFont.pointSize)
              }
        
       /* if let usedFont:UIFont = self.titleLabel!.font {
            log.debug(DEVICE_BOUNDS.width)
            if(SwifterSwift.isPad){
                self.titleLabel!.font = UIFont(name: usedFont.fontName, size: usedFont.pointSize * 1.5)
            }
            else{
                if(DEVICE_BOUNDS.width == 375){
                    self.titleLabel!.font = UIFont(name: usedFont.fontName, size: usedFont.pointSize * 1.10)
                }
                else if(DEVICE_BOUNDS.width == 414){
                    self.titleLabel!.font = UIFont(name: usedFont.fontName, size: usedFont.pointSize * 1.15)
                }
                else{
                    self.titleLabel!.font = UIFont(name: usedFont.fontName, size: usedFont.pointSize)
                }
            }
            
        }*/
        
    }
    @IBInspectable
    public var unserLine: Bool = false {
        didSet {
            
            if(unserLine){
                let attributedString = NSAttributedString(
                    string: (self.titleLabel?.text)!,
                    attributes:[
                        NSAttributedString.Key.font : self.titleLabel!.font,
                        NSAttributedString.Key.underlineColor : self.titleLabel!.textColor!,
                        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
                    ])
                self.setAttributedTitle(attributedString, for: .normal)
            }
        }
    }
    
    @IBInspectable
    public var nativeStyleDisabled: Bool = false {
        didSet {
            
            if(nativeStyleDisabled){
               self.borderColor = MHPColors.Color.HPLINKS_100
               self.borderWidth = 1
               self.cornerRadius = 6
               self.setTitleColor(MHPColors.Color.HPLINKS_100, for: UIControl.State.normal)
               self.backgroundColor = UIColor.white
            }
        }
    }
    @IBInspectable
    public var nativeStyleEnabled: Bool = false {
        didSet {
            
            if(nativeStyleEnabled){
                self.borderColor = MHPColors.Color.HPLINKS_100
                self.borderWidth = 1
                self.cornerRadius = 6
                self.setTitleColor(UIColor.white, for: UIControl.State.normal)
                self.backgroundColor = MHPColors.Color.HPLINKS_100
            }
        }
    }
    @IBInspectable
    public var styleDisabled: Bool = false {
        didSet {
            
            if(styleDisabled){
                self.cornerRadius = 3
                self.isUserInteractionEnabled = false
                self.setTitleColor(UIColor.white, for: UIControl.State.normal)
                self.backgroundColor = MHPColors.Color.PRIMARY_BUTTON_DISABLED
            }
        }
    }
    @IBInspectable
    public var styleRemitEnabled: Bool = false {
        didSet {
            
            if(styleRemitEnabled){
                self.cornerRadius = 3
                self.isUserInteractionEnabled = true
                self.setTitleColor(UIColor.white, for: UIControl.State.normal)
                self.backgroundColor = MHPColors.Color.REMIT_PRIMARY_BUTTON
            }
        }
    }
    
}
