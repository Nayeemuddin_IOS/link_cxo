//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

class MHPNotchLayoutConstraint: NSLayoutConstraint {

    //We use a simple inspectable to allow us to set a value for deviceHasTopNotch.
    @IBInspectable var Notch_Constraint: CGFloat
        {
        set{
            //Only apply value to device HasTopNotch.
//            if (SwifterSwift.deviceHasTopNotch)
//            {
//                self.constant = newValue;
//            }
        }
        get
        {
            return self.constant;
        }
    }
}
