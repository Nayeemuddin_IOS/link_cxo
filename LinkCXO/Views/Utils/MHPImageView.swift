//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

class MHPImageView: UIImageView {
    
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override var image: UIImage? {
        didSet {
            
        }
    }
    
    var universalImage: UIImage? {
        didSet {
            if(MHPUtil().isRTLEnabled){
                self.image = universalImage?.imageFlippedForRightToLeftLayoutDirection()
            }
            else{
                self.image = universalImage
            }
        }
    }

    
    func commonInit(){
        if(MHPUtil().isRTLEnabled){
            self.image = self.image?.imageFlippedForRightToLeftLayoutDirection()
        }
    }
}
