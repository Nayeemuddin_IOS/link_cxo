//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

class MHPPercentLayoutConstraint: NSLayoutConstraint {
    /// Layout constraint to calculate size based on multiplier.
        
        @IBInspectable var marginPercent: CGFloat = 0
    
        @IBInspectable var aspectMargin: CGFloat = 0
    
        @IBInspectable var ipadMargin: CGFloat = 0
    
        
        var screenSize: (width: CGFloat, height: CGFloat) {
            return (UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            self.layoutDidChangeWithWidth()
            self.layoutDidChangeWithIpadMargin()
            guard marginPercent > 0 else { return }
            self.layoutDidChange()
        }
        
        /**
         Re-calculate constant based on orientation and percentage.
         */
       @objc func layoutDidChange() {
            guard marginPercent > 0 else { return }
            
            switch firstAttribute {
            case .top, .topMargin, .bottom, .bottomMargin:
                constant = screenSize.height * marginPercent
            case .leading, .leadingMargin, .trailing, .trailingMargin:
                constant = screenSize.width * marginPercent
            default: break
            }
        }
    
    
    /**
     Re-calculate constant based on orientation and percentage.
     */
    @objc func layoutDidChangeWithWidth() {
        guard aspectMargin > 0 else { return }
        
        switch firstAttribute {
        case .top, .topMargin, .bottom, .bottomMargin:
            constant = aspectMargin * ASPECT_RATIO
        case .leading, .leadingMargin, .trailing, .trailingMargin:
            constant = aspectMargin * ASPECT_RATIO
        case .height:
            constant = aspectMargin * ASPECT_RATIO
        default: break
        }
    }
    
    
    /**
     Re-calculate constant based on orientation and percentage.
     */
    @objc func layoutDidChangeWithIpadMargin() {
        guard ipadMargin > 0 else { return }
        
        switch firstAttribute {
        case .top, .topMargin, .bottom, .bottomMargin:
            constant = ipadMargin
        case .leading, .leadingMargin, .trailing, .trailingMargin:
            constant = ipadMargin
        default: break
        }
    }
    
        
        deinit {
            guard marginPercent > 0 else { return }
            NotificationCenter.default.removeObserver(self)
        }
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
