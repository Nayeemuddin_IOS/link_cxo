//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

enum MHPLabelType: Int {
    case DEFAULT = 0
    case ERROR
}


class MHPLabel: UILabel {
    
    override func awakeFromNib() {
        
        if let usedFont:UIFont = self.font {
            //log.debug(DEVICE_BOUNDS.width)
            let uppercaseAttribs = [
                UIFontDescriptor.FeatureKey.featureIdentifier: kNumberCaseType,
                UIFontDescriptor.FeatureKey.typeIdentifier: kUpperCaseNumbersSelector
            ]

            let fontAttribs = [
                UIFontDescriptor.AttributeName.name: usedFont.fontName,
                UIFontDescriptor.AttributeName.featureSettings: [uppercaseAttribs]
                ] as [UIFontDescriptor.AttributeName : Any]

            let descriptor = UIFontDescriptor(fontAttributes: fontAttribs)
            self.font = UIFont(descriptor: descriptor, size: usedFont.pointSize)
        }
        
        if (self.text != nil){
            self.text = localizedStringFor(self.text!)
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        
//        if(MHPUtil().isRTLEnabled){
//            if(self.textAlignment == .left){
//                self.contentMode = .right
//            }
//        }
        
        //self.textAlignment =   NSTextAlignment.right
    }
    
    var localizedText: String? {
        didSet {
            if(localizedText != nil){
             self.text = localizedStringFor(localizedText!)
            }
            else{
             self.text = nil
            }
        }
    }
    
    public var LabelStyle : MHPLabelType? {
        
        didSet {
            switch self.LabelStyle! {
            case .DEFAULT:
                self.textColor = MHPColors.Color.HPTERTIARY_80
            case .ERROR:
                self.textColor = MHPColors.Color.ErrorColor
            }
        }
    }
    
    

    
    
}
