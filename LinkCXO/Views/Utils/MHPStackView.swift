//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

import UIKit

class MHPStackView: UIStackView {
    
    @IBInspectable var aspectSpacing: CGFloat = 0
    
    
    var screenSize: (width: CGFloat, height: CGFloat) {
        return (UIScreen.main.bounds.width, UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guard aspectSpacing > 0 else { return }
        self.layoutDidChange()
    }
    
    /**
     Re-calculate constant based on orientation and percentage.
     */
    @objc func layoutDidChange() {
        
        guard aspectSpacing > 0 else { return }
        let spacing = aspectSpacing * ASPECT_RATIO
        self.spacing = spacing
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


