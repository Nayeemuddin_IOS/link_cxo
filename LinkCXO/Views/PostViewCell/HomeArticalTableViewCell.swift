//
//  HomeArticalTableViewCell.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 07/10/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit


class HomeArticalTableViewCell:UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {

       // weak var cellDelegate:CustomCollectionCellDelegate? //define delegate
        @IBOutlet weak var myCollectionView: UICollectionView!
        //var aCategory:ImageCategory?
        let cellReuseId = "ArticlesCollectionViewCell"
        class var customCell : ArticlesCollectionViewCell {
            let cell = Bundle.main.loadNibNamed("ArticlesCollectionViewCell", owner: self, options: nil)?.last
            return cell as! ArticlesCollectionViewCell
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            //TODO: need to setup collection view flow layout
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.itemSize = CGSize(width: 100, height: 140)
            flowLayout.minimumLineSpacing = 2.0
            flowLayout.minimumInteritemSpacing = 5.0
            self.myCollectionView.collectionViewLayout = flowLayout
            
            //Comment if you set Datasource and delegate in .xib
            self.myCollectionView.dataSource = self
            self.myCollectionView.delegate = self
            
            //register the xib for collection view cell
            let cellNib = UINib(nibName: "ArticlesCollectionViewCell", bundle: nil)
            self.myCollectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseId)
        }
        
   
        //MARK: Collection view datasource and Delegate
    private func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) -> ArticlesCollectionViewCell {
            let cell = collectionView.cellForItem(at: indexPath) as? ArticlesCollectionViewCell
            cell!.layer.cornerRadius = 10
            cell!.layer.borderWidth = 0.2
            cell!.layer.borderColor = UIColor.lightGray.cgColor
           
        return cell!
        }
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
                return 5
            }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? ArticlesCollectionViewCell
            cell!.postView.cornerRadius = 10
            cell!.postView.layer.borderWidth = 0.5
            cell!.postView.layer.borderColor = UIColor.lightGray.cgColor
           
            return cell!
        }
        
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = 350//some width
        let height = 160 //ratio
        return CGSize(width: width, height: height)
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

}




