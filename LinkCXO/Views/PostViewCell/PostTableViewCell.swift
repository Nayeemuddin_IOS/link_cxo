//
//  PostTableViewCell.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 07/10/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit


class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var postView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 07, left: 0, bottom: 07, right: 0))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
