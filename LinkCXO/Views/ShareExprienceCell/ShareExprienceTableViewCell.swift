//
//  ShareExprienceTableViewCell.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 07/10/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class ShareExprienceTableViewCell: UITableViewCell {
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var profileButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
