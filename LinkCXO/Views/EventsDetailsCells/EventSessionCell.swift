//
//  EventSessionCell.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 19/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class EventSessionCell: UITableViewCell {

    @IBOutlet weak var event_Network_Status_lbl: UILabel!
    @IBOutlet weak var eventNetwork: UIView!
    @IBOutlet weak var eventLocation: MHPButton!
    @IBOutlet weak var eventTitle: MHPLabel!
    @IBOutlet weak var eventLogo: UIImageView!
    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
       }
    
    func setUPCellUI(){
        eventNetwork.layer.cornerRadius = eventNetwork.frame.size.height/2;
        eventNetwork.layer.masksToBounds = true;
    }
}
