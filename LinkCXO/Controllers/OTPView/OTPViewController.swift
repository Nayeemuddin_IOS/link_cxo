//
//  OTPViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 14/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit
import MZTimerLabel


class OTPViewController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var otp_timer_lbl: MZTimerLabel!
    @IBOutlet weak var otpContainerView: UIView!
    let otpStackView = OTPStackView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpOTPUI()

    }
    
    func setUpOTPUI(){
        
        otpContainerView.addSubview(otpStackView)
        otpStackView.delegate = self
        otpStackView.setAllFieldColor(isWarningColor: true, color: .yellow)
        otpStackView.heightAnchor.constraint(equalTo: otpContainerView.heightAnchor).isActive = true
        otpStackView.centerXAnchor.constraint(equalTo: otpContainerView.centerXAnchor).isActive = true
        otpStackView.centerYAnchor.constraint(equalTo: otpContainerView.centerYAnchor).isActive = true
    
       
        
    }
    
    
    @IBAction func navigationBackButtonPressed(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
          
      }
    
    override func viewWillAppear(_ animated: Bool) {
//        otpTextfield!.becomeFirstResponder()
//        otpTextfield!.delegate = self
    }
    

}
extension OTPViewController: OTPDelegate {
    
    func didChangeValidity(isValid: Bool) {
    
    }
    
}

    

