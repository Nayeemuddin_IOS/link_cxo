//
//  OTPTextField.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 14/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import Foundation
import UIKit

class OTPTextField: UITextField {
    
    weak var previousTextField: OTPTextField?
    weak var nextTextField: OTPTextField?
    
    override public func deleteBackward(){
        text = ""
        previousTextField?.becomeFirstResponder()
    }
    
}
