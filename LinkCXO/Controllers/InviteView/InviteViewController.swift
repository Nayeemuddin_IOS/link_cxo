//
//  InviteViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 23/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class InviteViewController: CXOBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI();
    }
    
    func setupUI(){
        self.showNavigationBar()
        
        self.navigationItem.title = localizedStringFor("Invite CXO")
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
