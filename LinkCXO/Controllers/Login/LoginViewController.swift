//
//  LoginViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 14/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var loginView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUoUI()

        // Do any additional setup after loading the view.
    }
    
    func setUoUI() {
        loginView.layer.borderWidth = 2;
        loginView.layer.borderColor = UIColor.white.cgColor
        loginView.layer.cornerRadius = 5;
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func loginNextButtonClick(_ sender: Any) {
        
        let loginOTPViewController : OTPViewController  = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
       // loginOTPViewController.userName = self.usernameTextField.text;
        self.navigationController?.pushViewController(loginOTPViewController, animated: true)
        
    }
}
