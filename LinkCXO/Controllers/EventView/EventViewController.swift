//
//  EventViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 17/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class EventViewController: CXOBaseViewController {
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        self.showNavigationBar()
        
        self.navigationItem.title = localizedStringFor("Events")
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.collectionView.registerCollectonCell(cellName:"EventsCollectionViewCell")
        // segmentController.addUnderlineForSelectedSegment()
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentController.changeUnderlinePosition()
    }
    
    func callEventDetailsView(){
        
        let loginOTPViewController : EventDetailsViewController  = Events_STORYBOARD.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        self.navigationController?.pushViewController(loginOTPViewController, animated: true)
        
    }
    
}
extension EventViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventsCollectionViewCell", for: indexPath) as! EventsCollectionViewCell
        // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventsCollectionViewCell", for: indexPath)
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 0.2
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 25 * 2) / 2 //some width
        let height = width * 1.5 //ratio
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.callEventDetailsView()
        
    }
    
    
    
}
