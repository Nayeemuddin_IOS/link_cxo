//
//  EventDetailsViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 19/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class EventDetailsViewController: CXOBaseViewController {
    
    @IBOutlet weak var tableView: SDStateTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI(){
        self.showNavigationBar()
        self.navigationItem.title = localizedStringFor("Event Details")
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        tableView.registerCellWithName(name : "EventSessionCell")
        tableView.registerCellWithName(name : "EventSessionRSVPCell")
        tableView.registerCellWithName(name : "EventAboutTableViewCell")
        tableView.registerCellWithName(name : "EventInvitTableViewCell")
        
    
        
    }
}
extension EventDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else  if section == 1{
            return 1
        }else  if section == 2{
            return 1
        }else  if section == 3{
            return 1
        }
        return 1;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventSessionCell", for: indexPath) as! EventSessionCell
            cell.setUPCellUI()
            
            return cell
        }else if indexPath.section == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventSessionRSVPCell", for: indexPath) as! EventSessionRSVPCell
            return cell
            
        }else if indexPath.section == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventAboutTableViewCell", for: indexPath) as! EventAboutTableViewCell
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventInvitTableViewCell", for: indexPath) as! EventInvitTableViewCell
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{
          return  UITableView.automaticDimension
        }else if indexPath.section == 1{
            return 170
        }else if indexPath.section == 2{
            return 300
        
        }else if indexPath.section == 3{
            return 100
        }
        else{
            return UITableView.automaticDimension
        }
        return  UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400 * ASPECT_RATIO  //+1 added for seperator
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    //update loadControl when user scrolls de tableView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
}
