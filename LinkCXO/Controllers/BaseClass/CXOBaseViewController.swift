//
//  CXOBaseViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class CXOBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func hideNavigationBar(){
          self.navigationController?.setNavigationBarHidden(true, animated: true)
       }
       
       func showNavigationBar(){
           self.navigationController?.setNavigationBarHidden(false, animated: false)
       }
    
    
//    let nib = UINib(nibName: "EventsCollectionViewCell", bundle: nil)
//    collectionView?.register(nib, forCellWithReuseIdentifier: "EventsCollectionViewCell")
    

   

}
public extension UICollectionView {
    func registerCollectonCell(cellName : String){
        let nib = UINib(nibName:cellName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: cellName)
        
    }
}
