//
//  HomeViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 17/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import UIKit

class HomeViewController: CXOBaseViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchView.layer.cornerRadius = 18
        }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideNavigationBar()
        self.tableView.registerCellWithName(name : "PostTableViewCell")
        self.tableView.registerCellWithName(name : "HomeNetworkTableViewCell")
        self.tableView.registerCellWithName(name : "HomeArticalTableViewCell")
        self.tableView.registerCellWithName(name : "ShareExprienceTableViewCell")
        
    }
    
    

}
extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 2
        }
     return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShareExprienceTableViewCell", for: indexPath) as! ShareExprienceTableViewCell
            cell.postView.cornerRadius = 4
            cell.postView.layer.borderWidth = 0.5
            cell.profileButton.cornerRadius = 25
            cell.profileButton.borderColor = #colorLiteral(red: 0.1201893762, green: 0.7036979198, blue: 0.4536497593, alpha: 1)
            cell.profileButton.borderWidth = 4
            cell.profileButton.clipsToBounds = true
            cell.postView.layer.borderColor = UIColor.lightGray.cgColor
            return cell
            
            
        }else if indexPath.section == 1{
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
            cell.postView.cornerRadius = 8
            cell.postView.layer.borderWidth = 0.5
            cell.postView.layer.borderColor = UIColor.lightGray.cgColor
            return cell
        }else if indexPath.section == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeArticalTableViewCell", for: indexPath) as! HomeArticalTableViewCell
           
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeNetworkTableViewCell", for: indexPath) as! HomeNetworkTableViewCell
            
            return cell
            
            
        }
    }
   
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 400 * ASPECT_RATIO
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            return 70 * ASPECT_RATIO
        }else if indexPath.section == 1{
            return 370 * ASPECT_RATIO
        }else if indexPath.section == 2{
            return 270 * ASPECT_RATIO
        }
        else{
            return 320 * ASPECT_RATIO
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
}


