//
//  SegmenteExtensions.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 18/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved

#if os(macOS)
import Cocoa
#elseif os(watchOS)
import WatchKit
#else
import UIKit
#endif

// MARK: - Properties
/// SwifterSwift: Common usefull properties and methods.
public struct SwifterSwift {
	
	#if !os(macOS)
	/// SwifterSwift: App's name (if applicable).
	static var appDisplayName: String? {
		// http://stackoverflow.com/questions/28254377/get-app-name-in-swift
		return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: App's bundle ID (if applicable).
	static var appBundleID: String? {
		return Bundle.main.bundleIdentifier
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: StatusBar height
	static var statusBarHeight: CGFloat {
		return UIApplication.shared.statusBarFrame.height
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: App current build number (if applicable).
	static var appBuild: String? {
		return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
	}
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Application icon badge current number.
	static var applicationIconBadgeNumber: Int {
		get {
			return UIApplication.shared.applicationIconBadgeNumber
		}
		set {
			UIApplication.shared.applicationIconBadgeNumber = newValue
		}
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: App's current version (if applicable).
	static var appVersion: String? {
		return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Current battery level.
	static var batteryLevel: Float {
		return UIDevice.current.batteryLevel
	}
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Shared instance of current device.
	static var currentDevice: UIDevice {
		return UIDevice.current
	}
	#elseif os(watchOS)
	/// SwifterSwift: Shared instance of current device.
	static var currentDevice: WKInterfaceDevice {
	return WKInterfaceDevice.current()
	}
	#endif
	
	
	#if !os(macOS)
	/// SwifterSwift: Screen height.
	static var screenHeight: CGFloat {
		#if os(iOS) || os(tvOS)
			return UIScreen.main.bounds.height
		#elseif os(watchOS)
			return currentDevice.screenBounds.height
		#endif
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: Current device model.
	static var deviceModel: String {
		return currentDevice.model
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: Current device name.
	static var deviceName: String {
		return currentDevice.name
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Current orientation of device.
	static var deviceOrientation: UIDeviceOrientation {
		return currentDevice.orientation
	}
	#endif
    
    #if os(iOS)
    static var deviceHasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    #endif
	
	#if !os(macOS)
	/// SwifterSwift: Screen width.
	static var screenWidth: CGFloat {
		#if os(iOS) || os(tvOS)
			return UIScreen.main.bounds.width
		#elseif os(watchOS)
			return currentDevice.screenBounds.width
		#endif
	}
	#endif
	
	/// SwifterSwift: Check if app is running in debug mode.
	static var isInDebuggingMode: Bool {
		// http://stackoverflow.com/questions/9063100/xcode-ios-how-to-determine-whether-code-is-running-in-debug-release-build
		#if DEBUG
			return true
		#else
			return false
		#endif
	}
	
	#if !os(macOS)
	/// SwifterSwift: Check if app is running in TestFlight mode.
	static var isInTestFlight: Bool {
		// http://stackoverflow.com/questions/12431994/detect-testflight
		return Bundle.main.appStoreReceiptURL?.path.contains("sandboxReceipt") == true
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Check if multitasking is supported in current device.
	static var isMultitaskingSupported: Bool {
		return UIDevice.current.isMultitaskingSupported
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Current status bar network activity indicator state.
	static var isNetworkActivityIndicatorVisible: Bool {
		get {
			return UIApplication.shared.isNetworkActivityIndicatorVisible
		}
		set {
			UIApplication.shared.isNetworkActivityIndicatorVisible = newValue
		}
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Check if device is iPad.
	static var isPad: Bool {
		return UIDevice.current.userInterfaceIdiom == .pad
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Check if device is iPhone.
	static var isPhone: Bool {
		return UIDevice.current.userInterfaceIdiom == .phone
	}
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Check if device is registered for remote notifications for current app (read-only).
	static var isRegisteredForRemoteNotifications: Bool {
		return UIApplication.shared.isRegisteredForRemoteNotifications
	}
	#endif
	
	/// SwifterSwift: Check if application is running on simulator (read-only).
	static var isRunningOnSimulator: Bool {
		// http://stackoverflow.com/questions/24869481/detect-if-app-is-being-built-for-device-or-simulator-in-swift
		#if (arch(i386) || arch(x86_64)) && (os(iOS) || os(watchOS) || os(tvOS))
			return true
		#else
			return false
		#endif
	}
	
	#if os(iOS)
	/// SwifterSwift: Status bar visibility state.
	static var isStatusBarHidden: Bool {
		get {
			return UIApplication.shared.isStatusBarHidden
		}
		set {
			UIApplication.shared.isStatusBarHidden = newValue
		}
	}
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Key window (read only, if applicable).
	static var keyWindow: UIView? {
		return UIApplication.shared.keyWindow
	}
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Most top view controller (if applicable).
	static var mostTopViewController: UIViewController? {
		get {
			return UIApplication.shared.keyWindow?.rootViewController
		}
		set {
			UIApplication.shared.keyWindow?.rootViewController = newValue
		}
	}
    
    static var visibleViewController: UIViewController? {
        if let topController = UIApplication.shared.keyWindow?.visibleViewController() {
            return topController
        }
        return nil
    }
    
    
  
	#endif
	
	#if os(iOS) || os(tvOS)
	/// SwifterSwift: Shared instance UIApplication.
	static var sharedApplication: UIApplication {
		return UIApplication.shared
	}
	#endif
	
	#if os(iOS)
	/// SwifterSwift: Current status bar style (if applicable).
	static var statusBarStyle: UIStatusBarStyle? {
		get {
			return UIApplication.shared.statusBarStyle
		}
		set {
			if let style = newValue {
				UIApplication.shared.statusBarStyle = style
			}
		}
	}
	#endif
	
	#if !os(macOS)
	/// SwifterSwift: System current version (read-only).
	static var systemVersion: String {
		return currentDevice.systemVersion
	}
	#endif
	
	/// SwifterSwift: Shared instance of standard UserDefaults (read-only).
	static var userDefaults: UserDefaults {
		return UserDefaults.standard
	}

	
}

// MARK: - Methods
public extension SwifterSwift {
	
	/// SwifterSwift: Delay function or closure call.
	///
	/// - Parameters:
	///   - milliseconds: execute closure after the given delay.
	///   - queue: a queue that completion closure should be executed on (default is DispatchQueue.main).
	///   - completion: closure to be executed after delay.
	///   - Returns: DispatchWorkItem task. You can call .cancel() on it to cancel delayed execution.
	@discardableResult static func delay(milliseconds: Double, queue: DispatchQueue = .main, completion: @escaping ()-> Void) -> DispatchWorkItem {
		let task = DispatchWorkItem { completion() }
		queue.asyncAfter(deadline: .now() + (milliseconds/1000), execute: task)
		return task
	}
	
	/// SwifterSwift: Debounce function or closure call.
	///
	/// - Parameters:
	///   - millisecondsOffset: allow execution of method if it was not called since millisecondsOffset.
	///   - queue: a queue that action closure should be executed on (default is DispatchQueue.main).
	///   - action: closure to be executed in a debounced way.
	static func debounce(millisecondsDelay: Int, queue: DispatchQueue = .main, action: @escaping (()->())) -> ()->() {
		//http://stackoverflow.com/questions/27116684/how-can-i-debounce-a-method-call
		var lastFireTime = DispatchTime.now()
		let dispatchDelay = DispatchTimeInterval.milliseconds(millisecondsDelay)
		
		return {
			let dispatchTime: DispatchTime = lastFireTime + dispatchDelay
			queue.asyncAfter(deadline: dispatchTime) {
				let when: DispatchTime = lastFireTime + dispatchDelay
				let now = DispatchTime.now()
				if now.rawValue >= when.rawValue {
					lastFireTime = DispatchTime.now()
					action()
				}
			}
		}
	}
	
	/// SwifterSwift: Object from UserDefaults.
	///
	/// - Parameter forKey: key to find object for.
	/// - Returns: Any object for key (if exists).
	static func object(forKey: String) -> Any? {
		return UserDefaults.standard.object(forKey: forKey)
	}
	
	/// SwifterSwift: String from UserDefaults.
	///
	/// - Parameter forKey: key to find string for.
	/// - Returns: String object for key (if exists).
	static func string(forKey: String) -> String? {
		return UserDefaults.standard.string(forKey: forKey)
	}
	
	/// SwifterSwift: Integer from UserDefaults.
	///
	/// - Parameter forKey: key to find integer for.
	/// - Returns: Int number for key (if exists).
	static func integer(forKey: String) -> Int? {
		return UserDefaults.standard.integer(forKey: forKey)
	}
	
	/// SwifterSwift: Double from UserDefaults.
	///
	/// - Parameter forKey: key to find double for.
	/// - Returns: Double number for key (if exists).
	static func double(forKey: String) -> Double? {
		return UserDefaults.standard.double(forKey: forKey)
	}
	
	/// SwifterSwift: Data from UserDefaults.
	///
	/// - Parameter forKey: key to find data for.
	/// - Returns: Data object for key (if exists).
	static func data(forKey: String) -> Data? {
		return UserDefaults.standard.data(forKey: forKey)
	}
	
	/// SwifterSwift: Bool from UserDefaults.
	///
	/// - Parameter forKey: key to find bool for.
	/// - Returns: Bool object for key (if exists).
	static func bool(forKey: String) -> Bool? {
		return UserDefaults.standard.bool(forKey: forKey)
	}
	
	/// SwifterSwift: Array from UserDefaults.
	///
	/// - Parameter forKey: key to find array for.
	/// - Returns: Array of Any objects for key (if exists).
	static func array(forKey: String) -> [Any]? {
		return UserDefaults.standard.array(forKey: forKey)
	}
	
	/// SwifterSwift: Dictionary from UserDefaults.
	///
	/// - Parameter forKey: key to find dictionary for.
	/// - Returns: ictionary of [String: Any] for key (if exists).
	static func dictionary(forKey: String) -> [String: Any]? {
		return UserDefaults.standard.dictionary(forKey: forKey)
	}
	
	/// SwifterSwift: Float from UserDefaults.
	///
	/// - Parameter forKey: key to find float for.
	/// - Returns: Float number for key (if exists).
	static func float(forKey: String) -> Float? {
		return UserDefaults.standard.object(forKey: forKey) as? Float
	}
	
	/// SwifterSwift: Save an object to UserDefaults.
	///
	/// - Parameters:
	///   - value: object to save in UserDefaults.
	///   - forKey: key to save object for.
	static func set(_ value: Any?, forKey: String) {
		UserDefaults.standard.set(value, forKey: forKey)
	}

	/// SwifterSwift: Class name of object as string.
	///
	/// - Parameter object: Any object to find its class name.
	/// - Returns: Class name for given object.
	static func typeName(for object: Any) -> String {
		let type = Swift.type(of: object.self)
		return String.init(describing: type)
	}
    
    /// pop back to specific viewcontroller
    public func popBack<T: UIViewController>(toControllerType: T.Type, _ completion : ((Bool)-> (Void))? = nil) {
        
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        
        if var viewControllers: [UIViewController] = navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                navigationController?.popToViewController(currentViewController, animated: false)
                    completion?(true)
                   return
                }
            }
        }
        
        
//        if let topVC = SwifterSwift.mostTopViewController {
//            if let controller = topVC as? ExSlideMenuController{
//                if let navigationController = controller.mainViewController as? UINavigationController{
//                    var viewControllers: [UIViewController] = navigationController.viewControllers
//                    viewControllers = viewControllers.reversed()
//                    for currentViewController in viewControllers {
//                        if currentViewController .isKind(of: toControllerType) {
//                       navigationController.popToViewController(currentViewController, animated: false)
//                            completion?(true)
//                            return
//                        }
//                    }
//                }
//            }
//        }
        
         completion?(false)
        
    }
    
    
//    /// pop back to specific viewcontroller
//    public func isCurrentViewController<T: UIViewController>(ControllerType: T.Type )->Bool {
//
//
//        if let topVC = SwifterSwift.mostTopViewController {
//            if let controller = topVC as? ExSlideMenuController{
//                if let navigationController = controller.mainViewController as? UINavigationController{
//                    let viewControllers: [UIViewController] = navigationController.viewControllers
//                    if (viewControllers.count > 0){
//                        if viewControllers.last!.isKind(of: ControllerType){
//                            return true
//                        }
//                    }
//
//                }
//            }
//        }
//         return false
//    }
//
    
//
//    static var currentViewController: UIViewController?{
//
//
//        if let topVC = SwifterSwift.mostTopViewController {
//            if let controller = topVC as? ExSlideMenuController{
//                if let navigationController = controller.mainViewController as? UINavigationController{
//                    let viewControllers: [UIViewController] = navigationController.viewControllers
//                    if (viewControllers.count > 0){
//                        return viewControllers[viewControllers.count - 1]
//                    }
//
//                }
//            }
//        }
//        return nil
//    }
    
    
//    static var currentNavigationController: UINavigationController?
//    {
//        if let topVC = SwifterSwift.mostTopViewController {
//            if let controller = topVC as? ExSlideMenuController{
//                if let navigationController = controller.mainViewController as? UINavigationController{
//                   return navigationController
//                }
//            }
//        }
//        return nil
//    }
	
}
