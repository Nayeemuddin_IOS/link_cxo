//
//  UITableViewExtensions.swift
//  Malaicha
//
//  Created by vishnu khaitan on 11/07/18..
//  Copyright © 2018 Diasporatec LTD. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit


// MARK: - Properties
public extension UITableView {
	
	/// SwifterSwift: Index path of last row in tableView.
	var indexPathForLastRow: IndexPath? {
		return indexPathForLastRow(inSection: lastSection)
	}
	
	/// SwifterSwift: Index of last section in tableView.
	var lastSection: Int {
		return numberOfSections > 0 ? numberOfSections - 1 : 0
	}
	
	/// SwifterSwift: Number of all rows in all sections of tableView.
	var numberOfRows: Int {
		var section = 0
		var rowCount = 0
		while section < numberOfSections {
			rowCount += numberOfRows(inSection: section)
			section += 1
		}
		return rowCount
	}
	
}
    
// MARK: - Methods
public extension UITableView {

	/// SwifterSwift: IndexPath for last row in section.
	///
	/// - Parameter section: section to get last row in.
	/// - Returns: optional last indexPath for last row in section (if applicable).
	func indexPathForLastRow(inSection section: Int) -> IndexPath? {
		guard section >= 0 else {
			return nil
		}
		guard numberOfRows(inSection: section) > 0  else {
			return IndexPath(row: 0, section: section)
		}
		return IndexPath(row: numberOfRows(inSection: section) - 1, section: section)
	}
	
	/// Reload data with a completion handler.
	///
	/// - Parameter completion: completion handler to run after reloadData finishes.
	func reloadData(_ completion: @escaping () -> Void) {
		UIView.animate(withDuration: 0, animations: {
			self.reloadData()
		}, completion: { _ in
			completion()
		})
	}
	
	/// SwifterSwift: Remove TableFooterView.
	func removeTableFooterView() {
		tableFooterView = nil
	}
	
	/// SwifterSwift: Remove TableHeaderView.
	func removeTableHeaderView() {
		tableHeaderView = nil
	}
	
	
	/// SwifterSwift: Scroll to bottom of TableView.
	///
	/// - Parameter animated: set true to animate scroll (default is true).
	func scrollToBottom(animated: Bool = true) {
		let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height)
		setContentOffset(bottomOffset, animated: animated)
	}
	
	/// SwifterSwift: Scroll to top of TableView.
	///
	/// - Parameter animated: set true to animate scroll (default is true).
	func scrollToTop(animated: Bool = true) {
		setContentOffset(CGPoint.zero, animated: animated)
	}
    
    /// SwifterSwift: Dequeue reusable UITableViewCell using class name
    ///
    /// - Parameter name: UITableViewCell type
    /// - Returns: UITableViewCell object with associated class name (optional value)
    func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type) -> T? {
        return dequeueReusableCell(withIdentifier: String(describing: name)) as? T
    }

	/// SwiferSwift: Dequeue reusable UITableViewCell using class name for indexPath
	///
	/// - Parameters:
	///   - name: UITableViewCell type.
	///   - indexPath: location of cell in tableView.
	/// - Returns: UITableViewCell object with associated class name.
	func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: String(describing: name), for: indexPath) as! T
    }
    
    /// SwiferSwift: Dequeue reusable UITableViewHeaderFooterView using class name
    ///
    /// - Parameter name: UITableViewHeaderFooterView type
    /// - Returns: UITableViewHeaderFooterView object with associated class name (optional value)
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(withClass name: T.Type) -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: String(describing: name)) as? T
    }

	/// SwifterSwift: Register UITableViewHeaderFooterView using class name
	///
	/// - Parameters:
	///   - nib: Nib file used to create the header or footer view.
	///   - name: UITableViewHeaderFooterView type.
	func register<T: UITableViewHeaderFooterView>(nib: UINib?, withHeaderFooterViewClass name: T.Type) {
        register(nib, forHeaderFooterViewReuseIdentifier: String(describing: name))
    }
    
    /// SwifterSwift: Register UITableViewHeaderFooterView using class name
    ///
    /// - Parameter name: UITableViewHeaderFooterView type
    func register<T: UITableViewHeaderFooterView>(headerFooterViewClassWith name: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: String(describing: name))
    }
    
    /// SwifterSwift: Register UITableViewCell using class name
    ///
    /// - Parameter name: UITableViewCell type
    func register<T: UITableViewCell>(cellWithClass name: T.Type) {
        register(T.self, forCellReuseIdentifier: String(describing: name))
    }

	/// SwifterSwift: Register UITableViewCell using class name
	///
	/// - Parameters:
	///   - nib: Nib file used to create the tableView cell.
	///   - name: UITableViewCell type.
    func register<T: UITableViewCell>(nib: UINib?, withCellClass name: T.Type) {
        register(nib, forCellReuseIdentifier: String(describing: name))
    }
    
    func registerCellWithName(name : String) {
         self.register(UINib.init(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
    
    
    
    
   
    
    
    func setUpTableViewEmptyStateView(){
        if let tableView  = self as? SDStateTableView{
            tableView.stateLabelTitleFontFamily = FontName.RalewayExtraBold.rawValue
            tableView.stateLabelSubtitleFontSize = 14 * ASPECT_RATIO
            tableView.stateLabelSubtitleFontFamily = FontName.RobotoLight.rawValue
            tableView.stateViewSubtitleColor = MHPColors.Color.HPTEXTINACTIVE ?? UIColor.lightGray
            tableView.stateViewTitleColor = MHPColors.Color.HPTEXTACTIVE_ORANGE ?? UIColor.gray
            let button = MHPRoundButton()
            button.ButtonStyle = .PRIMARY_ENABLED
            tableView.actionButton = button
            tableView.buttonColor = .white
            tableView.buttonBorderColor = MHPColors.Color.HPTEXTACTIVE_ORANGE ?? UIColor.lightGray
            tableView.buttonSize = CGSize(width: 279, height: 48)
        }
        
    }
	
}
#endif
