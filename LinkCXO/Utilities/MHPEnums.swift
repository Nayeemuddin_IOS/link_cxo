//
//  MHPEnums.swift
//  Malaicha
//
//  Created by vishnu khaitan on 08/08/18.
//  Copyright © 2018 Diasporatec LTD. All rights reserved.
//

import UIKit

enum IdTypes : Int {
    case RSAID = 0  , Passport = 1 , Asylum = 2,RSAIDLITE = 3  , PassportLITE = 4 , AsylumLITE = 5, Other = 6
}

enum ErrorCode : Int {
    case NoProducts = 10001,NoProductsSearched = 10002,EmptyCart = 10003,NoRecipient = 10004,NoDeliveryMethods = 10005,NoTransactions = 10006, NoMoreProducts = 10007, Forbidden = 403
}

enum ServerErrorCode : Int{
    case InternalServerError = 3100001
    case ServerError = 3100007
    case DTServerError = 3100006
    case DTTimeOutError = 3100008
    case DTServerError1 = -2000
    case DTTimeOutError1 = -3000
    static var allValues = [InternalServerError.rawValue, InternalServerError.rawValue,DTServerError.rawValue,DTTimeOutError.rawValue,DTServerError1.rawValue,DTTimeOutError1.rawValue]
}

enum PaymentHistoryCodes : String{
    case NonRecurringPayment = "TT00091"
    case NonRecurringPaymentInternal = "TT00143"
    case ThirdPartyPayment = "TT03177"
    case OnceOffPayment = "TT00074"
    case OnceOffPaymentInternal = "TT00156"
    case VasMoneySendRegistered = "TT00632" // P2P
    static var allValues = [NonRecurringPayment.rawValue,NonRecurringPaymentInternal.rawValue, ThirdPartyPayment.rawValue, OnceOffPayment.rawValue, OnceOffPaymentInternal.rawValue, VasMoneySendRegistered.rawValue]
}

enum DcoumentUploadNotificationStatusCodes : String{
    case SOF_DOCS_FAILED = "SOF_DOCS_FAILED"
    case HP_DOCS_FAILED = "HP_DOCS_FAILED"
    case SOF_DOCS_SUCCESS = "SOF_DOCS_SUCCESS"
    case HP_DOCS_SUCCESS = "HP_DOCS_SUCCESS"
    static var allValues = [SOF_DOCS_FAILED.rawValue,HP_DOCS_FAILED.rawValue, SOF_DOCS_SUCCESS.rawValue, HP_DOCS_SUCCESS.rawValue]
 }

enum NotificationOptions : Int {
    case SMS = 0  , Email, Both
}

enum NotificationType : String {
    case SMS , Email, None
}

enum PaymentFlow : Int {
    case None = 0, AddRecipient,AddPublicRecipient,PayRecipient,OnceOffPayment,P2PPayment,P2PRequest,ManageRecipient,InternalTransfer
}

enum DocumentSource : Int {
    case None = 0, Camera = 1  , Gallery = 2, Documents = 3
}

enum TransactionType : Int {
    case None = 0, All,Credit,Debit
}

enum TransactionFilter : String {
    case All,Cleared,Uncleared
}

enum StatementFlow : Int {
    case None = 0, PaymentProof,StatementHistory
}

enum CardFlow : Int {
    case None = 0, ChangePin,SetPin,StopCard,RequestCard
}

enum SetPinType: Int {
    case None = 0, SetAppPin,ResetAppPin,SetCardPin,ChangeCardPin
}

enum CardDeliveryMethod : Int {
    case None = 0, AgentRequest,HelloPaisaStore
}

enum CardStatus : String, Codable {
    case UNLINKED, LINKED,ACTIVE
}

enum InfoNotification : Int {
    case None = 0, CallBack, NetworkError, ServerMaintenance, TimeOut,Success,failure, SessionExpired, UnavailableService,UnderMaintenance, ForbiddenService,TimetoLogin,CallBackLead, ForceUpdate, isHigherVersionAvailable, MaximumOTPResendsReached,PinReset,RecipientAdded,RecipientAddedForOtherWareHouse,CallMeInfo
}

enum InfoPreview : Int {
    case None = 0, NoRecipient, NoTransactions,NoStatements,NoNetwork, Error
}

enum ConfirmationAlert : Int {
    case None = 0, Logout,MaximumAttemptsReached,Download, ExitProcess, Success, Failure,NetworkError,DeleteRecipient,OutOfStock,NoProduct,OutOfStockItems,LessInStockItems,WareHouseConfirmation,PayFromBankWithOutstandingAmount,PayFromBank,OrderNotFound,InvalidOffer,BankingApp,OfferExpired
}


   enum Tab : Int {
       case HOME = 0
       case SHOP
       case CART
       case TRANSACTION_HISTORY
       case MORE
   }

enum Products : Int {
    case None = 0, Banking , SendMoney, Cover, More
}

enum ProductType : String {
 case InStock,Avalibale,OutOfStock

}
enum HistyoryType : String {
 case Name,Payment,Address,OrderStatus

}
enum HistoryPaymenyStatus {
    case Cancel
}

enum OTPType : Int {
    case Login,Banking
}

enum OTPServiceCode : String {
    case DEFAULT_OTP, CUSTOMER_VERIFICATION_OTP,TRANSACTION_OTP,FORGOT_PASSWORD_OTP,MSISDN_CHECK_OTP,WALLET_CHECK_OTP,LOGIN_WITH_OTP,ADD_BENEFICIARY_OTP,CHANGE_CARD_PIN_OTP,SET_CARD_PIN_OTP,CHANGE_APP_PIN_OTP,SET_APP_PIN_OTP,EDIT_BENEFICIARY_OTP,ACC_TRANSFER_OTP,ADD_NON_RECURRING_BENEFICIARY_OTP,EDIT_NON_RECURRING_BENEFICIARY_OTP
}




enum CreateUserStep : Int {
    case None = 0,UserName,SetAppPin,ResetAppPin
}

enum FontName: String {
    case RobotoBlack            = "Roboto-Black"
    case RobotoBold             = "Roboto-Bold"
    case RobotoLight            = "Roboto-Light"
    case RobotoMedium           = "Roboto-Medium"
    case RobotoRegular          = "Roboto-Regular"
    case RobotoThin             = "Roboto-Thin"
    case RalewayBlack            = "Raleway-Black"
    case RalewayBold             = "Raleway-Bold"
    case RalewaySemiBold         = "Raleway-Semibold"
    case RalewayExtraBold        = "Raleway-ExtraBold"
    case RalewayLight            = "Raleway-Light"
    case RalewayMedium           = "Raleway-Medium"
    case RalewayRegular          = "Raleway-Regular"
    case RalewayThin             = "Raleway-Thin"
}



enum StandardSize: CGFloat {
    case h1 = 20.0
    case h2 = 18.0
    case h3 = 16.0
    case h4 = 14.0
    case h5 = 12.0
    case h6 = 10.0
}

enum Environment: String {
    case SANDBOX       = "SANDBOX"
    case QA            = "QA"
    case STAGING       = "STAGING"
    case PROD          = "PROD"
}

class MHPEnums: NSObject {

}


enum RecipentType : String {
    case ThirdParty = "ThirdParty", NonRecurring = "NonRecurring"
}


enum PaymentDetailType : Int {
    case Amount = 0, RecipientStatementDes, StatementDes
}

enum EditRecipientType : Int {
    case RecipientBank = 0, AccountType, RecipientDetail, NotificationInfo, StatementDes, SectionCount
}

enum TextFieldType : String {
    case RecipientName = "RecipientName", Amount = "Amount" , AccountNumber = "AccountNumber", Email = "Email", Phone = "Phone", RecipientReference = "RecipientReference", OwnReference = "OwnReference"
}

enum DateType : Int {
        case None = 0 , ToDate, FromDate
}

enum DocTypeCode : String {
        case Signature = "SIGNAT", ProofOfPayment = "PMTPRF", Selfie = "SEFFIE", RSAID = "RSAIDP", Passport = "PASPRT",ASYLUM = "ASYLUM"
}
