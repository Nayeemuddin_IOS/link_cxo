//
//  EventViewController.swift
//  LinkCXO
//
//  Created by Shaik Nayeemuddin on 17/09/20.
//  Copyright © 2020 LinkCXO. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    
    // UIView class function to create an view with
    // translateAutoResizingMaskIntoConstraints is disabled
    class func autolayoutView() -> Self {
        let view = self.init()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}

public extension UIEdgeInsets {
     init(margin: CGFloat) {
        self.init()
        self.top = margin
        self.bottom = margin
        self.left = margin
        self.right = margin
    }
     init(sidePadding: CGFloat, verticalPadding: CGFloat) {
        self.init()
        self.top = verticalPadding
        self.bottom = verticalPadding
        self.left = sidePadding
        self.right = sidePadding
    }
}
